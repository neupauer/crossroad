﻿(function () {
  'use strict';

  // Semaphore
  function Semaphore(id) {
    this.red = document.getElementById(id + "-red");
    this.yellow = document.getElementById(id + "-yellow");
    this.green = document.getElementById(id + "-green");

    this.isOn = false;
    this.isOff = true;
    this.isService = false;
    this.serviceInterval = null;

    this.on = this.on.bind(this);
    this.off = this.off.bind(this);
    this.reset = this.reset.bind(this);

    this.reset();
  }

  Semaphore.prototype.reset = function() {
    this.isOn = false;
    this.isOff = true;
    this.isService = false;
    this.serviceInterval = null;
    this.red.style.display = "block";
    this.yellow.style.display = "none";
    this.green.style.display = "none";
  };

  Semaphore.prototype.on = function() {
    var self = this;
    if (this.isOn) { return; }

    this.isOn = true;
    this.isOff = false;

    self.yellow.style.display = "block";

    setTimeout(function() {
      self.red.style.display = "none";
      self.yellow.style.display = "none";
      self.green.style.display = "block";
    }, 1000);
  };

  Semaphore.prototype.off = function() {
    var self = this;
    if (this.isOff) { return; }

    this.isOn = false;
    this.isOff = true;

    self.yellow.style.display = "block";

    setTimeout(function() {
      self.red.style.display = "block";
      self.yellow.style.display = "none";
      self.green.style.display = "none";
    }, 1000);
  };

  Semaphore.prototype.serviceOn = function() {
    var self = this;
    if (this.isService) { return; }

    this.isService = true;

    self.yellow.style.display = "none";

    self.red.style.display = "none";
    self.green.style.display = "none";

    self.serviceInterval = setInterval(function() {
      self.yellow.style.display = "block";
      self.red.style.display = "none";
      self.green.style.display = "none";

      setTimeout(function() {
        self.yellow.style.display = "none";
      }, 500);
    }, 1000);
  };

  Semaphore.prototype.serviceOff = function() {
    var self = this;
    if (!this.isService) { return; }
    clearInterval(self.serviceInterval);
    this.reset();
  };

  // Semaphore
  function WalkerSemaphore(id) {
    this.light = document.getElementById(id + "-light");

    this.isOn = false;
    this.isOff = true;

    this.on = this.on.bind(this);
    this.off = this.off.bind(this);
    this.reset = this.reset.bind(this);

    this.reset();
  }

  WalkerSemaphore.prototype.reset = function() {
    this.isOn = false;
    this.isOff = true;
    this.light.style.fill = "#e74c3c";
  };

  WalkerSemaphore.prototype.on = function() {
    if (this.isOn) { return; }

    this.isOn = true;
    this.isOff = false;

    this.light.style.fill = "#27ae60";
  };

  WalkerSemaphore.prototype.off = function() {
    if (this.isOff) { return; }

    this.isOn = false;
    this.isOff = true;

    this.light.style.fill = "#e74c3c";
  };

  WalkerSemaphore.prototype.serviceOn = function() {
    var self = this;
    self.light.style.display = "none";
  };

  WalkerSemaphore.prototype.serviceOff = function() {
    var self = this;
    self.light.style.display = "block";
  };

  //      
  // An event handler can take an optional event argument
  // and should not return a value
                                            
                                                                 

  // An array of all currently registered event handlers for a type
                                              
                                                              
  // A map of event types and their corresponding event handlers.
                          
                                   
                                     
    

  /** Mitt: Tiny (~200b) functional event emitter / pubsub.
   *  @name mitt
   *  @returns {Mitt}
   */
  function mitt(all                 ) {
    all = all || Object.create(null);

    return {
        /**
         * Register an event handler for the given type.
         *
         * @param  {String} type    Type of event to listen for, or `"*"` for all events
         * @param  {Function} handler Function to call in response to given event
         * @memberOf mitt
         */
        on: function on(type        , handler              ) {
            (all[type] || (all[type] = [])).push(handler);
        },

        /**
         * Remove an event handler for the given type.
         *
         * @param  {String} type    Type of event to unregister `handler` from, or `"*"`
         * @param  {Function} handler Handler function to remove
         * @memberOf mitt
         */
        off: function off(type        , handler              ) {
            if (all[type]) {
                all[type].splice(all[type].indexOf(handler) >>> 0, 1);
            }
        },

        /**
         * Invoke all handlers for the given type.
         * If present, `"*"` handlers are invoked after type-matched handlers.
         *
         * @param {String} type  The event type to invoke
         * @param {Any} [evt]  Any value (object is recommended and powerful), passed to each handler
         * @memberOf mitt
         */
        emit: function emit(type        , evt     ) {
            (all[type] || []).slice().map(function (handler) { handler(evt); });
            (all['*'] || []).slice().map(function (handler) { handler(type, evt); });
        }
    };
  }

  var emitter = mitt();

  var CHANGE_EVENT = "CHANGE_EVENT";

  var Store = {
    getState: function getState() {
      return (
        axios
          // .get("https://localhost:5002/api/state")
          .get("http://neupauer.azurewebsites.net/api/state")
          .then(function (response) { return response.data; })
      );
    },

    setState: function setState(state) {
      return (
        axios
          // .post("https://localhost:5002/api/state/" + state)
          .post("http://neupauer.azurewebsites.net/api/state/" + state)
          .then(function (response) { return response.data; })
      );
    },

    getMode: function getMode() {
      return (
        axios
          // .get("https://localhost:5002/api/mode")
          .get("http://neupauer.azurewebsites.net/api/mode")
          .then(function (response) { return response.data; })
      );
    },

    setMode: function setMode(mode) {
      return (
        axios
          // .post("https://localhost:5002/api/mode/" + mode)
          .post("http://neupauer.azurewebsites.net/api/mode/" + mode)
          .then(function (response) { return response.data; })
      );
    },

    getService: function getService() {
      return (
        axios
          // .get("https://localhost:5002/api/service")
          .get("http://neupauer.azurewebsites.net/api/service")
          .then(function (response) { return response.data; })
      );
    },

    setService: function setService(service) {
      return (
        axios
          // .post("https://localhost:5002/api/service/" + service)
          .post("http://neupauer.azurewebsites.net/api/service/" + service)
          .then(function (response) { return response.data; })
      );
    },

    dispatchIndex: function (payload) {
      switch (payload.type) {
        case "TYPE":
          break;
        default:
          throw 'Payloda type "' + payload.type + '" is not defined.';
      }
    },

    emitChangeListener: function () {
      emitter.emit(CHANGE_EVENT);
    },

    addChangeListener: function (callback) {
      emitter.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function (callback) {
      emitter.off(CHANGE_EVENT, callback);
    }
  };

  var btnModeAuto = document.getElementById("modeAuto");
  var btnModeManual = document.getElementById("modeManual");

  var btnStateAC = document.getElementById("stateAC");
  var btnStateBD = document.getElementById("stateBD");

  var btnServiceOn = document.getElementById("serviceOn");
  var btnServiceOff = document.getElementById("serviceOff");

  var manualControls = document.getElementById("manualControls");
  var controls = document.getElementById("controls");

  var state = 0;
  var service = 0;

  setInterval(function() {
    if (service == 1) {
      controls.style.display = "none";
    } else {
      controls.style.display = "block";
    }

    Store.getMode().then(function (data) {
      if (data == "1") {
        // auto
        btnModeAuto.className = "is-active";
        btnModeManual.className = "";

        manualControls.style.display = "none";
      } else {
        // manual
        btnModeManual.className = "is-active";
        btnModeAuto.className = "";

        manualControls.style.display = "block";
      }

      console.log(("Traffic Light Mode: " + (data == "1" ? "AUTOMATIC" : "MANUAL")));
    });

    Store.getState().then(function (data) {
      if (data == "0") {
        if (state != "0") {
          btnStateAC.className = "is-active";
          btnStateBD.className = "";

          Traffic._stateAC();
        }
        state = 0;
      } else {
        if (state != "1") {
          btnStateBD.className = "is-active";
          btnStateAC.className = "";

          Traffic._stateBD();
        }
        state = 1;
      }

      console.log(("Traffic Light State: " + (data == "0" ? "AC" : "BD")));
    });

    Store.getService().then(function (data) {
      if (data == "0") {
        if (service != "0") {
          btnServiceOff.className = "is-active";
          btnServiceOn.className = "";
          Traffic._serviceOff();
        }
        service = 0;
      } else {
        if (service != "1") {
          btnServiceOn.className = "is-active";
          btnServiceOff.className = "";
          Traffic._serviceOn();
        }
        service = 1;
      }

      console.log(("Service: " + (data == "0" ? "OFF" : "ON")));
    });
  }, 1000);

  window.Traffic = (function() {
    var semaphoreA = new Semaphore("semaphore-a");
    var semaphoreB = new Semaphore("semaphore-b");
    var semaphoreC = new Semaphore("semaphore-c");
    var semaphoreD = new Semaphore("semaphore-d");

    var walkerSemaphoreA = new WalkerSemaphore("semaphore-wa");
    var walkerSemaphoreB = new WalkerSemaphore("semaphore-wb");

    function on(semaphore) {
      switch (semaphore) {
        case 1:
        case "a":
        case "A":
          semaphoreA.on();
          return;
        case 2:
        case "b":
        case "B":
          semaphoreB.on();
          return;
        case 3:
        case "c":
        case "C":
          semaphoreC.on();
          return;
        case 4:
        case "d":
        case "D":
          semaphoreD.on();
          return;
        default:
          console.warn(
            ("Semaphore: \"" + semaphore + "\" does not exists! Available are A, B, C, D.")
          );
      }
    }

    function off(semaphore) {
      switch (semaphore) {
        case 1:
        case "a":
        case "A":
          semaphoreA.off();
          return;
        case 2:
        case "b":
        case "B":
          semaphoreB.off();
          return;
        case 3:
        case "c":
        case "C":
          semaphoreC.off();
          return;
        case 4:
        case "d":
        case "D":
          semaphoreD.off();
          return;
        default:
          console.warn(
            ("Semaphore: \"" + semaphore + "\" does not exists! Available are A, B, C, D.")
          );
      }
    }

    function state(s) {
      switch (s) {
        case 0:
        case "ac":
        case "ca":
        case "AC":
        case "CA":
          Store.setState(0);
          return;
        case 1:
        case "bd":
        case "db":
        case "BD":
        case "DB":
          Store.setState(1);
          return;
        default:
          console.warn(
            ("State: \"" + s + "\" does not exists! Available are 0, 1 or AC, BD.")
          );
      }
    }

    function mode(m) {
      switch (m) {
        case 0:
        case "m":
        case "manual":
          Store.setMode(0);
          return;
        case 1:
        case "a":
        case "auto":
        case "automatic":
          Store.setMode(1);
          return;
        default:
          console.warn(
            ("Mode: \"" + m + "\" does not exists! Available are 0, 1 or manual, auto.")
          );
      }
    }

    function service(s) {
      switch (s) {
        case 0:
        case "off":
          Store.setService(0);
          Traffic.mode(1);
          return;
        case 1:
        case "on":
          Store.setService(1);
          Traffic.mode(0);
          return;
        default:
          console.warn(
            ("Service: \"" + s + "\" does not exists! Available are 0, 1 or off, on.")
          );
      }
    }

    function _stateAC() {
      semaphoreB.off();
      semaphoreD.off();
      setTimeout(function() {
        semaphoreA.on();
        semaphoreC.on();
        walkerSemaphoreA.on();
        walkerSemaphoreB.on();

        semaphoreB.off();
        semaphoreD.off();
      }, 2000);
    }

    function _stateBD() {
      semaphoreA.off();
      semaphoreC.off();
      walkerSemaphoreA.off();
      walkerSemaphoreB.off();
      setTimeout(function() {
        semaphoreB.on();
        semaphoreD.on();

        semaphoreA.off();
        semaphoreC.off();
        walkerSemaphoreA.off();
        walkerSemaphoreB.off();
      }, 2000);
    }

    function _serviceOn() {
      semaphoreA.serviceOn();
      semaphoreB.serviceOn();
      semaphoreC.serviceOn();
      semaphoreD.serviceOn();
      walkerSemaphoreA.serviceOn();
      walkerSemaphoreB.serviceOn();
    }

    function _serviceOff() {
      semaphoreA.serviceOff();
      semaphoreB.serviceOff();
      semaphoreC.serviceOff();
      semaphoreD.serviceOff();
      walkerSemaphoreA.serviceOff();
      walkerSemaphoreB.serviceOff();
    }

    return {
      semaphoreOn: on,
      semaphoreOff: off,
      state: state,
      mode: mode,
      service: service,
      _stateAC: _stateAC,
      _stateBD: _stateBD,
      _serviceOn: _serviceOn,
      _serviceOff: _serviceOff
    };
  })();

  window.walker = function() {
    axios.get("http://neupauer.azurewebsites.net");
  };

}());
