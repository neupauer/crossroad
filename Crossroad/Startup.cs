﻿using System;
using System.IO;
using System.Threading;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;

namespace Crossroad
{
    public class Startup
    {
        // 0 = manual, 1 = automatic
        public static int trafficLightMode = 0;

        Thread crossroad;

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRouting();

            //services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            //{
            //    builder.AllowAnyOrigin()
            //           .AllowAnyMethod()
            //           .AllowAnyHeader();
            //}));

            // Crossroad Semaphore State
            StreamWriter w;
            w = new StreamWriter("State.txt");
            w.WriteLine("0");
            w.Close();

            // Walker Semaphore State
            w = new StreamWriter("Walker.txt");
            w.WriteLine("0");
            w.Close();

            // Walker Semaphore State
            w = new StreamWriter("Service.txt");
            w.WriteLine("0");
            w.Close();

            crossroad = new Thread(new ThreadStart(InvokeMethod));
            crossroad.Start();

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseRouter(router =>
            {
                router.MapGet("api/state", async (request, response, routeData) =>
                {
                    StreamReader reader = new StreamReader("State.txt");
                    string s = reader.ReadLine();
                    reader.Close();
                    response.Headers.Append("Access-Control-Allow-Origin", "*");
                    await response.WriteAsync(s);

                });

                router.MapGet("api/traffic-state", async (request, response, routeData) =>
                {
                    StreamReader reader = new StreamReader("State.txt");
                    string s = reader.ReadLine();
                    reader.Close();
                    response.Headers.Append("Access-Control-Allow-Origin", "*");
                    await response.WriteAsync("0".Equals(s) ? "1" : "2");

                });

                router.MapGet("api/mode", async (request, response, routeData) =>
                {
                    response.Headers.Append("Access-Control-Allow-Origin", "*");
                    await response.WriteAsync(trafficLightMode.ToString());
                });

                router.MapPost("api/mode/{number:int}", async (request, response, routeData) =>
                {
                    string mode = routeData.Values["number"].ToString();
                    trafficLightMode = "1".Equals(mode) ? 1 : 0;
                    response.Headers.Append("Access-Control-Allow-Origin", "*");
                    await response.WriteAsync("Done!");
                });

                router.MapPost("api/state/{number:int}", async (request, response, routeData) =>
                {
                    if (trafficLightMode == 0) { // manual
                        string state = routeData.Values["number"].ToString();
                        state = "1".Equals(state) ? "1" : "0";
                        StreamWriter writer = new StreamWriter("State.txt");
                        writer.WriteLine(state);
                        writer.Close();
                    }

                    response.Headers.Append("Access-Control-Allow-Origin", "*");
                    await response.WriteAsync("Done!");
                });

                router.MapGet("api/walker", async (request, response, routeData) =>
                {
                    StreamWriter writer = new StreamWriter("Walker.txt");
                    writer.WriteLine("1");
                    writer.Close();
                    response.Headers.Append("Access-Control-Allow-Origin", "*");
                    await response.WriteAsync("Done!");
                });

                router.MapGet("api/service", async (request, response, routeData) =>
                {
                    StreamReader reader = new StreamReader("Service.txt");
                    string s = reader.ReadLine();
                    reader.Close();
                    response.Headers.Append("Access-Control-Allow-Origin", "*");
                    await response.WriteAsync(s);
                });

                router.MapPost("api/service/{number:int}", async (request, response, routeData) =>
                {
                    string state = routeData.Values["number"].ToString();
                    state = "1".Equals(state) ? "1" : "0";
                    StreamWriter writer = new StreamWriter("Service.txt");
                    writer.WriteLine(state);
                    writer.Close();
              
                    response.Headers.Append("Access-Control-Allow-Origin", "*");
                    await response.WriteAsync("Done!");
                });
            });
        }

        static void InvokeMethod()
        {
            int counter = 0;
            while (true)
            {


                StreamReader reader = new StreamReader("Walker.txt");
                string walker = reader.ReadLine();
                reader.Close();

                if("1".Equals(walker)) {

                    StreamWriter writer = new StreamWriter("State.txt");
                    writer.WriteLine("0");
                    writer.Close();

                    writer = new StreamWriter("Walker.txt");
                    writer.WriteLine("0");
                    writer.Close();

                    Thread.Sleep(5000);
                }

                if (trafficLightMode == 1 && counter > 10) {
                    counter = 0;
                    reader = new StreamReader("State.txt");
                    string newState = "";

                    if ("1".Equals(reader.ReadLine()))
                    {
                        newState = "0";
                    }
                    else
                    {
                        newState = "1";
                    }

                    reader.Close();

                    StreamWriter writer = new StreamWriter("State.txt");
                    writer.WriteLine(newState);
                    writer.Close();
                                
                }

                counter += 1;
                Thread.Sleep(1000);
            }
        }

    }
}
